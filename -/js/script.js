$().ready(function(
/* Author: 
Dan Holloran
copyright 2011
*/

//Set Global Vars
	var date = new Date(),
	currentDay = date.getDate(),
	currentMonth = date.getMonth(),
	currentYear = date.getFullYear(),
	textMonth = '',
	months = ['January', 'Feburary', 'March', 
	          'April', 'May', 'June', 'July', 
	          'August', 'September', 'November', 'December'],
	hours = date.getHours(),
	minutes = date.getMinutes(),
	seconds = date.getSeconds(),
	thumbImageLink = 'https://s3.amazonaws.com/DKdesigns_images/body/project_thumbs/'
	amPm =''
	;

	//Set Month to text
	function setMonthString(){
		textMonth = months[currentMonth];
		return textMonth;
	}
	
	//Set Full Date 
	function setFullDate(){
		setMonthString();
		var html = textMonth + " " 
				   + currentDay + ", " 
	               + currentYear;
		document.getElementById('date').innerHTML = html; 
	}

	//Setup Clock
	function startTime()
	{
		var today=new Date();
		var h=today.getHours();
		var m=today.getMinutes();
		// add a zero in front of numbers<10
		m=checkTime(m);
		h=checkHours(h);
		document.getElementById('clock').innerHTML=h+":"+m+amPm;
		t=setTimeout('startTime()',500);
	}

	function checkHours(i){
		if(i >= 13){
			i = i - 12;
			amPm = 'pm'
		}else if(i===0){
			i=12;
			amPm = 'am'	
		}
		return i;
	}
	
	function checkTime(i)
	{
		if (i<10)
		{
			i="0" + i;
		}
		return i;
	}
	
//Check time of day	
	function checkTimeofDay(){
		if( hours >= 6 && hours < 12){
			document.getElementById('greeting').innerHTML = 'Good morning, ';
		}else if( hours >= 12 && hours < 17){
			document.getElementById('greeting').innerHTML = 'Good after noon, ';
		}else if(hours >= 17 && hours < 24){
			//document.getElementById('greeting').innerHTML = 'Good evening, ';
		}else{
			document.getElementById('greeting').innerHTML = 'Hello night owl, ';
		}		
	}

	//Portfolio Project Thumbnail Objects
	var weatherLink = thumbImageLink + 'weather_app_thumb.jpg',
		weatherApp = [	'Flash ActionScript 3.0 Weather App' , 
					  	weatherLink, 
					  	'portfolio/WeatherApp.php',
					  	'Weather App',
					  	'#',
					  	'Weather Application'
					 ],
	
		audioLink = thumbImageLink + 'audio_app_thumb.jpg'
		audioApp = [	'Flash ActionScript 3.0 Audio App' , 
		            	audioLink, 
			  		'portfolio/AudioApp.php',
			  		'Audio App',
			  		'#',
			  		'Audio Application'
			 		],
		
		drawingLink = thumbImageLink + 'drawing_app_thumb.jpg'
		drawingApp = [	'Flash ActionScript 3.0 Drawing App' , 
			  			drawingLink, 
			  			'portfolio/DrawingApp.php',
			  			'Drawing App',
			  			'#',
			  			'Drawing Application'
			 		 ],
		picViewLink = thumbImageLink + 'pic_viewer_thumb.jpg'
		picView = [	'Flash ActionScript 3.0 Picture Viewer' , 
						picViewLink, 
						'portfolio/PicView.php',
			  			'Picture Viewer',
						'#',
						'Picture Viewer'
					 ],
		dkvxMediaLink = thumbImageLink + 'dkvx_media_thumb.jpg',
		dkvxMedia = [	'DKVX Media' , 
		              	dkvxMediaLink, 
		              	'portfolio/dkvx_media/index.html',
		              	'DKVX Media',
		              	'#',
		              	'DKVX Media'
		              	],
		photoZoomLink = thumbImageLink + 'photozoom_thumb.jpg',
		photoZoom = [	'PhotoZoom Air Application' ,
		             	photoZoomLink,
		             	'portfolio/photo_zoom/index.html',
		             	'PhotoZoom Air Application',
		             	'#',
		             	'PhotoZoom Air Application'
		],
		wwnLink = thumbImageLink + 'wwn_site_thumb.jpg',
		wwnSite = [	'World Wide News' ,
		             	wwnLink,
		             	'portfolio/wwn/index.html',
		             	'World Wide News',
		             	'#',
		             	'World Wide News'
		]
		
	;
	
	//Portfolio Project Thumbnail Vars
	var projects =[weatherApp, audioApp, 
	               drawingApp, picView, 
	               dkvxMedia, photoZoom,
	               wwnSite],
		alt = '',
		src = '',
		link = '',
		title = '',
		git = '',
		heading = '',
		thumbs = []
	;

	//Write the HTML for the thumbnails
	function writeThumbHtml(a,s,l,t,g,h){
		 var txt = '<div class="project_info">' +
		 '<h2>' + h + '</h2>' +
		'<img alt="'+ a + '" src="' + s + '" title="' + t + '" width="260" height="160" />' +
		'<div class="buttons">' +
		'<span><a href="'+ l +'" class="button">View Project</a></span>' +
		'<span><a href="' + g + '" class="button">View Source</a></span>' +
		'</div> <!-- END of #buttons DIV !-->' +
		'</div> <!-- END of #project_info DIV !-->'
		thumbs.push(txt);
}
	
	//Create Portfolio Project Thumbnails
	function createProjectThumbnails(){
		for(var i = 0, j = projects.length; i<j ; i++ ){
			
			alt = projects[i][0];
			src = projects[i][1];
			link = projects[i][2];
			title = projects[i][3];
			git = projects[i][4];
			heading = projects[i][5];
			
			writeThumbHtml(alt, src, link, title, git, heading);
		} 
		var text = thumbs.toString();
		var html = text.replace(/,/gi,'');
		document.getElementById('project_thumbs').innerHTML = html;
	}
	if(document.getElementById('greeting')){
		checkTimeofDay();
	}	
	setFullDate();
	startTime();
	if(document.getElementById('project_thumbs')){
		createProjectThumbnails();
	}	

){});



















